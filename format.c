#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define PROGRAM_NAME "format"

void usage(FILE *fd){
    fprintf(fd, "Usage: %s [-f] [-i informat] [-o outformat]\n", PROGRAM_NAME);
}

int main(int argc, char **argv){
    
    enum type {
        FLOAT,
        LONG
    } input_type;

    char *in_format;
    in_format = (char *) malloc(256);
    strcpy(in_format, "%d");
    
    char *out_format;
    out_format = (char *) malloc(256);
    strcpy(out_format, "%d"); 
    
    input_type = LONG;

    opterr = 0;
    int c;

    while ((c = getopt(argc, argv, "hlfi:o:")) != -1){
        switch (c){
            case 'h':
                usage(stdout);
                return 0;
                break;
            case 'i':
                strcpy(in_format, optarg);
                break;
            case 'l':
                input_type = LONG;
                break;
            case 'f':
                input_type = FLOAT;
                break;
            case 'o':
                strcpy(out_format, optarg);
                break;
            default:
                usage(stderr);
                return 1;
        }
    }

    strcat(out_format, "\n");

    if (input_type == FLOAT){
        float input;

        while (scanf(in_format, &input) != EOF){
            printf(out_format, input);
        }
    } else {
        long input;

        while (scanf(in_format, &input) != EOF){
            printf(out_format, input);
        }
    }

    free(in_format);
    free(out_format);
    
    return 0;
}
