CC=clang
CFLAGS=-Wall

all: format

format: format.o
	$(CC) -o format format.o

install:
	install man/format.1.gz /usr/local/share/man/man1/
	install format /usr/local/bin/format

clean:
	rm *.o format
